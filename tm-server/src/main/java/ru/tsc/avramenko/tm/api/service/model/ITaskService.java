package ru.tsc.avramenko.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Task changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @Nullable
    Task changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

    @Nullable
    Task changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Task findByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task findByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    void removeByName(@Nullable String userId, @Nullable String name);

    @Nullable
    void removeByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @Nullable
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    Task startById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task startByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Task startByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Task finishById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task finishByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Task finishByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task findById(@Nullable String userId, @Nullable String id);

    @Nullable
    void removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<Task> findAll(@Nullable String userId);

    @Nullable
    List<Task> findAll();

    void clear(@Nullable String userId);

    void clear();

    void addAll(@Nullable List<Task> tasks);

}