package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.avramenko.tm.api.endpoint.ISessionEndpoint;
import ru.tsc.avramenko.tm.api.service.dto.ISessionDtoService;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @Autowired
    private ISessionDtoService sessionDtoService;

    @Nullable
    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password
    ) {
        return sessionDtoService.open(login, password);
    }

    @Nullable
    @WebMethod
    public boolean closeSession(
            @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionDtoService.validate(session);
        try {
            sessionDtoService.close(session);
            return true;
        } catch (@NotNull final AccessDeniedException e) {
            return false;
        }
    }

}