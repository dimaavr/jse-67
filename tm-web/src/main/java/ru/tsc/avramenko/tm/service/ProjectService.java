package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.avramenko.tm.api.service.IProjectService;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.repository.ProjectRepository;

import java.util.Collection;

@Service
public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private ProjectRepository repository;

    @Override
    public @Nullable Project findById(@Nullable String id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public @Nullable Collection<Project> findAll() {
        return repository.findAll();
    }

    @Override
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public @NotNull Project create(@Nullable Project project) {
        return repository.save(project);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String id) {
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public @NotNull Project save(@NotNull Project project) {
        return repository.save(project);
    }

}