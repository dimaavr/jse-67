package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.avramenko.tm.api.service.ITaskService;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.repository.TaskRepository;

import java.util.Collection;

@Service
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository repository;

    @Override
    public @Nullable Task findById(@Nullable String id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public @Nullable Collection<Task> findAll() {
        return repository.findAll();
    }

    @Override
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public @NotNull Task create(@Nullable Task task) {
        return repository.save(task);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String id) {
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public @NotNull Task save(@NotNull Task task) {
        return repository.save(task);
    }

}